package com.tk2323.ftsm.a162021_a161520_a157787.measureupcalories;

import android.provider.BaseColumns;

public class ConsumeContract {

    public ConsumeContract() {

    }

    public static final class ConsumeEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "consume";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_CALORIES = "calories";
        public static final String COLUMN_TYPE = "type";
        public static final String COLUMN_BARCODE = "barcode";
    }
}
