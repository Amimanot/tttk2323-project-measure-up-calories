package com.tk2323.ftsm.a162021_a161520_a157787.measureupcalories;

import android.content.Intent;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    int totalCal, foodCal, drinkCal;
    int recCal = 2300;
    int dailyCalPercentage = (totalCal/recCal) * 100;
    TextView tv_totalCal, tv_foodCal, tv_drinkCal, tv_percent;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar();;
        setupUI();
    }

    private void setupToolbar()
    {
        Toolbar tb_main = findViewById(R.id.tb_main);
        setSupportActionBar(tb_main);
        final ActionBar myActionBar = getSupportActionBar();
        myActionBar.setDisplayHomeAsUpEnabled(false);
    }

    private void setupUI()
    {

        ImageButton btn_calorie = findViewById(R.id.img_calorie);
        ImageButton btn_food = findViewById(R.id.img_food);
        ImageButton btn_drink = findViewById(R.id.img_drink);

        FloatingActionButton fab = findViewById(R.id.btn_float);

        tv_totalCal = findViewById(R.id.txt_calorie_intake);
        tv_foodCal = findViewById(R.id.txt_food_calorie);
        tv_drinkCal = findViewById(R.id.txt_drink_calorie);
        tv_percent = findViewById(R.id.txt_percentage);

        tv_totalCal.setText(totalCal + " kcal");
        tv_foodCal.setText(foodCal + " kcal");
        tv_drinkCal.setText(drinkCal + " kcal");
        tv_percent.setText(dailyCalPercentage + "%");

        btn_calorie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent total = new Intent(MainActivity.this, DetailActivity.class);
                total.putExtra("Type", "Total");
                startActivity(total);
            }
        });

        btn_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent food = new Intent(MainActivity.this, DetailActivity.class);
                food.putExtra("Type", "Food");
                startActivity(food);
            }
        });

        btn_drink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent drink = new Intent(MainActivity.this, DetailActivity.class);
                drink.putExtra("Type", "Drink");
                startActivity(drink);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent search = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(search);
            }
        });
    }
}
