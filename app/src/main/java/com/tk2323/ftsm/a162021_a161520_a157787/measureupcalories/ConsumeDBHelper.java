package com.tk2323.ftsm.a162021_a161520_a157787.measureupcalories;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.tk2323.ftsm.a162021_a161520_a157787.measureupcalories.ConsumeContract.*;

public class ConsumeDBHelper extends SQLiteOpenHelper {
        private static final int DATABASE_VERSION = 1;
        static final String DATABASE_NAME = "ConsumeDataBase.db";

        private static final String SQL_CREATE_CONSUME_TABLE =
                "CREATE TABLE " + ConsumeEntry.TABLE_NAME + " (" +
                        ConsumeEntry._ID + " INTEGER PRIMARY KEY, " +
                        ConsumeEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                        ConsumeEntry.COLUMN_CALORIES + " INTEGER NOT NULL, " +
                        ConsumeEntry.COLUMN_TYPE + " INTEGER NOT NULL, " +
                        ConsumeEntry.COLUMN_BARCODE + " TEXT NOT NULL" +
                        " );";

        public ConsumeDBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_CONSUME_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + ConsumeEntry.TABLE_NAME);
            onCreate(db);
        }
}
