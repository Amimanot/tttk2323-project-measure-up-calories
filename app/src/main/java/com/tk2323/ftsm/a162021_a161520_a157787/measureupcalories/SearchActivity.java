package com.tk2323.ftsm.a162021_a161520_a157787.measureupcalories;

import android.content.Intent;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    TextView barcodeNum;
    ArrayList<String> barcodes;
    ListView lv_barcode;

    @Override
    protected void onResume() {
        super.onResume();
        //loadData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        lv_barcode = findViewById(R.id.lv_searchResult);
        barcodeNum = findViewById(R.id.tv_barNum);
        setupToolbar();
        setupBarcodeScanner();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            String contents = intent.getStringExtra("SCAN_RESULT");
            barcodeNum.setText(contents.toString());

        }
    }

    private void setupToolbar()
    {
        Toolbar tb_search = findViewById(R.id.tb_search);
        setSupportActionBar(tb_search);
        final ActionBar myActionBar = getSupportActionBar();
        myActionBar.setDisplayHomeAsUpEnabled(true);
        myActionBar.setTitle("Search");
    }

    private void setupBarcodeScanner() {
        Button btn_scan = findViewById(R.id.btn_camscan);

        btn_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(SearchActivity.this);
                integrator.initiateScan();
            }
        });
    }

    public void loadData()
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1,barcodes);

        lv_barcode.setAdapter(adapter);
    }
}
