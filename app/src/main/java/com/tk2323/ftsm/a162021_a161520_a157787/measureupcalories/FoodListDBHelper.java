package com.tk2323.ftsm.a162021_a161520_a157787.measureupcalories;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.tk2323.ftsm.a162021_a161520_a157787.measureupcalories.FoodListContract.*;


public class FoodListDBHelper extends SQLiteOpenHelper {
    
    private static final int DATABASE_VERSION = 1;
    static final String DATABASE_NAME = "FoodListDataBase.db";

    private static final String SQL_CREATE_FOOD_LIST_TABLE =
            "CREATE TABLE " + FoodListEntry.TABLE_NAME + " (" +
                    FoodListEntry._ID + " INTEGER PRIMARY KEY, " +
                    FoodListEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                    FoodListEntry.COLUMN_CALORIES + " INTEGER NOT NULL, " +
                    FoodListEntry.COLUMN_TYPE + " INTEGER NOT NULL, " +
                    FoodListEntry.COLUMN_BARCODE + " TEXT " +
                    " );";

    public FoodListDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_FOOD_LIST_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + FoodListEntry.TABLE_NAME);
        onCreate(db);
    }
}
