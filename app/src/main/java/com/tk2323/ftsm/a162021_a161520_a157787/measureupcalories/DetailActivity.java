package com.tk2323.ftsm.a162021_a161520_a157787.measureupcalories;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.widget.ListView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    ListView lv_detail;
    String type, value;
    TextView title, calories;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        setupToolbar();

        lv_detail = findViewById(R.id.lv_food);

        type = getIntent().getStringExtra("Type");
        value = getIntent().getStringExtra("Value");

        title = findViewById(R.id.tv_appTitle);
        calories = findViewById(R.id.tv_calNum);

        title.setText("Your " + type + " Calories");
        calories.setText(value + " kcal");
    }

    private void setupToolbar()
    {
        Toolbar tb_detail = findViewById(R.id.tb_detail);
        setSupportActionBar(tb_detail);
        final ActionBar myActionBar = getSupportActionBar();
        String type = getIntent().getStringExtra("Type");
        myActionBar.setDisplayHomeAsUpEnabled(true);
        myActionBar.setTitle(type + " Detail");
    }
}
